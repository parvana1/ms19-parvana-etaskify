package az.ingress.springbootsecurity.enums;

public enum Role {
    USER,
    ADMIN
}
