package az.ingress.springbootsecurity.enums;

public enum Status {
    ACTIVATED,
    DEACTIVATED
}
