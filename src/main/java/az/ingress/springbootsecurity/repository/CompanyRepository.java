package az.ingress.springbootsecurity.repository;

import az.ingress.springbootsecurity.model.Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface CompanyRepository extends JpaRepository<Company,Long> {
    Optional<Company> findByName(String name);
    Optional<Company> findByPhone(String name);

    Optional<Company> findById(Long Id);

//    @Query(value="select c from Company c left join fetch m.branches  where m.id=:id")
//    Market findByIdd(Long id);



}
