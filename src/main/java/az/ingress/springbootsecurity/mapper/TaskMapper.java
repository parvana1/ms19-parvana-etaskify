package az.ingress.springbootsecurity.mapper;

import az.ingress.springbootsecurity.dto.CreateTaskDto;
import az.ingress.springbootsecurity.dto.TaskDto;
import az.ingress.springbootsecurity.model.Task;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TaskMapper {
    List<TaskDto> listEntityToDto(List<Task> tasks);
    TaskDto entityToDto(Task task);

}
