package az.ingress.springbootsecurity.mapper;

import az.ingress.springbootsecurity.dto.UserDto;
import az.ingress.springbootsecurity.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;


@Mapper(componentModel = "spring")
public interface UserMapper {
    List<UserDto> listEntityToDto(List<User> users);

    UserDto entityToDto(User entity);
    User dtoToEntity(UserDto dto);

}
