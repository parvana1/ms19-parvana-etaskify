package az.ingress.springbootsecurity.mapper;

import az.ingress.springbootsecurity.dto.CompanyDto;
import az.ingress.springbootsecurity.model.Company;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CompanyMapper {
    CompanyDto entityToDto(Company company);
    Company  dtoToEntity(CompanyDto companyDto);
    List<CompanyDto> listEntityToDto(List<Company> companyList);


}
