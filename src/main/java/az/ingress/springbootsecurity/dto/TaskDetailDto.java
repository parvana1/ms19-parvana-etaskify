package az.ingress.springbootsecurity.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TaskDetailDto {
    private String title;
    private String description;
    private LocalDate deadline;
    private  String status;
    private CompanyDto companyDto;


}
