package az.ingress.springbootsecurity.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ResponseSignUpDto {
    private String name;
    private String phone;
    private String address;
    private String username;
    private String email;
}
