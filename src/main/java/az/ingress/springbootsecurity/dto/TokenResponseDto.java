package az.ingress.springbootsecurity.dto;

import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TokenResponseDto {
    String accessToken;
    String refreshToken;
}
