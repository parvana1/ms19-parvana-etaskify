package az.ingress.springbootsecurity.service;


import az.ingress.springbootsecurity.dto.UserDto;
import az.ingress.springbootsecurity.dto.UserProfileDto;

public interface UserService {
    UserDto createUser(UserDto userDto);
    UserProfileDto profile();
    UserDto  findById(Long id);
    UserDto update(Long id, UserDto userDto);
   void  delete(Long id);
}
