package az.ingress.springbootsecurity.service;

import az.ingress.springbootsecurity.config.security.JwtService;
import az.ingress.springbootsecurity.dto.LoginRequestDto;
import az.ingress.springbootsecurity.dto.TokenResponseDto;
import az.ingress.springbootsecurity.exception.ErrorCodes;
import az.ingress.springbootsecurity.exception.NotFoundException;
import az.ingress.springbootsecurity.exception.PasswordMismatch;
import az.ingress.springbootsecurity.model.Company;
import az.ingress.springbootsecurity.model.User;
import az.ingress.springbootsecurity.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthService {
    private final UserRepository userRepository;
    private final PasswordEncoder encoder;
    private final JwtService jwtService;
   public  TokenResponseDto login(LoginRequestDto dto) {
       User user=userRepository.findByName(dto.getUsername()).orElseThrow(()->new NotFoundException(ErrorCodes.USER_NOT_FOUND.name()));
       if (encoder.matches(dto.getPassword(), user.getPassword())) {
            return jwtService.issueToken(user);
        } else {
            throw new PasswordMismatch(ErrorCodes.PASSWORD_MISMATCH.name());
        }
    }
}
