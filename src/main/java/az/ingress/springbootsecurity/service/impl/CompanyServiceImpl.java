package az.ingress.springbootsecurity.service.impl;

import az.ingress.springbootsecurity.config.security.JwtService;
import az.ingress.springbootsecurity.dto.*;
import az.ingress.springbootsecurity.enums.Role;
import az.ingress.springbootsecurity.exception.*;
import az.ingress.springbootsecurity.mapper.CompanyMapper;
import az.ingress.springbootsecurity.mapper.TaskMapper;
import az.ingress.springbootsecurity.mapper.UserMapper;
import az.ingress.springbootsecurity.model.Company;
import az.ingress.springbootsecurity.model.Task;
import az.ingress.springbootsecurity.model.User;
import az.ingress.springbootsecurity.repository.CompanyRepository;
import az.ingress.springbootsecurity.repository.TaskRepository;
import az.ingress.springbootsecurity.repository.UserRepository;
import az.ingress.springbootsecurity.service.CompanyService;
import az.ingress.springbootsecurity.util.PasswordValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CompanyServiceImpl implements CompanyService {
    private final CompanyRepository companyRepository;
    private final UserRepository userRepository;
    private final TaskRepository taskRepository;
    private final CompanyMapper companyMapper;
    private final TaskMapper taskMapper;
    private final UserMapper userMapper;
    private final JwtService jwtService;
    private final PasswordEncoder encoder;
    private  final PasswordValidator passwordValidator;


    @Override
    public CompanyDto getCompanyById(Long id) {
        Company company=companyRepository.findById(id).orElseThrow(()->new NotFoundException("Company not found"));
        return companyMapper.entityToDto(company);
    }

    @Override
    public CompanyDto update(Long id,CompanyDto companyDto) {
        User signedUser=jwtService.getSignedUser();
        if(signedUser.getCompany().getId()!=id && !signedUser.getRole().equals(Role.ADMIN.toString())){
            throw  new MissingPermission(ErrorCodes.MISSING_PERMISSION.name());
        }
        Company company=companyRepository.findById(id).orElseThrow(()->new NotFoundException("Company not found"));
        if(companyDto.getName()!=null){
            company.setName(companyDto.getName());
        }
        if(companyDto.getAddress()!=null){
            company.setAddress(companyDto.getAddress());
        }
        if(companyDto.getPhone()!=null){
            company.setAddress(companyDto.getAddress());
        }
        companyRepository.save(company);

        return companyMapper.entityToDto(company);
    }

    @Override
    public void delete(Long id) {
        User signedUser=jwtService.getSignedUser();
        if(signedUser.getCompany().getId()!=id && !signedUser.getRole().equals(Role.ADMIN.toString())){
            throw  new MissingPermission(ErrorCodes.MISSING_PERMISSION.name());
        }
        Company company=companyRepository.findById(id).orElseThrow(()->new NotFoundException(ErrorCodes.COMPANY_NOT_FOUND.name()));
        companyRepository.delete(company);
    }

    @Override
    public ResponseSignUpDto signUp(SignUpDto signUpDto) {
        if(!passwordValidator.isValid(signUpDto.getPassword())) {
         throw new PasswordMismatch("Only allow passwords with 6 or more \n" +
                 "alphanumeric characters");
        }
        Optional<User> byEmail = userRepository.findByEmail(signUpDto.getEmail());
        if (byEmail.isPresent()) {
            throw new AlreadyExistException(ErrorCodes.EMAIL_EXIST.name());
        }
        Optional<Company> byName = companyRepository.findByName(signUpDto.getName());
        if (byName.isPresent()) {

            throw new AlreadyExistException(ErrorCodes.COMPANY_NAME_EXIST.name());
        }
        Optional<Company> byPhone = companyRepository.findByPhone(signUpDto.getPhone());
        if (byPhone.isPresent()) {
            throw new AlreadyExistException(ErrorCodes.PHONE_NUMBER_EXIST.name());
        }

        Company company= Company.builder()
                .name(signUpDto.getName())
                .address(signUpDto.getAddress())
                .phone(signUpDto.getPhone())
                .build();
        companyRepository.save(company);

        User admin= User.builder()
                .email(signUpDto.getEmail())
                .password(encoder.encode(signUpDto.getPassword()))
                .name(signUpDto.getUsername())
                .role(Role.ADMIN)
                .company(company)
                .build();
         userRepository.save(admin);
        ResponseSignUpDto responseSignUpDto= ResponseSignUpDto.builder()
                .name(company.getName())
                .address(company.getAddress())
                .phone(company.getPhone())
                .email(admin.getEmail())
                .username(admin.getName())
                .build();
        return responseSignUpDto;
    }

    @Override
    public List<CompanyDto> findAll() {
        List<Company> companyList=companyRepository.findAll();
        if(companyList==null){
            throw  new NotFoundException("");
        }

        return companyMapper.listEntityToDto(companyList);

    }

    @Override
    public List<TaskDto> getTasks(Long id) {
        Company company=companyRepository.findById(id).orElseThrow(()->new NotFoundException(ErrorCodes.COMPANY_NOT_FOUND.name()));
        List<Task> tasks=taskRepository.findByCompanyId(company.getId());
        if(tasks==null) {
            throw new NotFoundException("This company tasks not found......");
        }
        return taskMapper.listEntityToDto(tasks);
    }

    @Override
    public List<UserDto> getUsers(Long id) {
        Company company=companyRepository.findById(id).orElseThrow(()->new NotFoundException(ErrorCodes.COMPANY_NOT_FOUND.name()));
        return userMapper.listEntityToDto(company.getUsers());
    }


}
