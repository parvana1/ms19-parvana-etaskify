package az.ingress.springbootsecurity.service.impl;


import az.ingress.springbootsecurity.config.security.JwtService;
import az.ingress.springbootsecurity.dto.UserDto;
import az.ingress.springbootsecurity.dto.UserProfileDto;
import az.ingress.springbootsecurity.enums.Role;
import az.ingress.springbootsecurity.exception.AlreadyExistException;
import az.ingress.springbootsecurity.exception.ErrorCodes;
import az.ingress.springbootsecurity.exception.NotFoundException;
import az.ingress.springbootsecurity.mapper.CompanyMapper;
import az.ingress.springbootsecurity.mapper.UserMapper;
import az.ingress.springbootsecurity.model.Company;
import az.ingress.springbootsecurity.model.User;
import az.ingress.springbootsecurity.repository.UserRepository;
import az.ingress.springbootsecurity.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final CompanyMapper companyMapper;
    private final UserMapper userMapper;
    private final PasswordEncoder encoder;
    private final JwtService jwtService;
    private static final String DEFAULT_PASSWORD = "a56Z98K";

    @Override
    public UserDto createUser(UserDto userDto) {
       Optional<User> user = userRepository.findByEmail(userDto.getEmail());
        if (user.isPresent()) {
            throw new AlreadyExistException(ErrorCodes.ALREADY_EXIST.name());
        }else{
            User currentUser=jwtService.getSignedUser();
            User createdUser= User.builder()
                    .name(userDto.getName())
                    .surname(userDto.getSurname())
                    .password(encoder.encode(DEFAULT_PASSWORD))
                    .email(userDto.getEmail())
                    .company(currentUser.getCompany())
                    .role(Role.USER)
                    .build();
            userRepository.save(createdUser);
        }

        return null;
    }

    @Override
    public UserProfileDto profile() {
        User user=jwtService.getSignedUser();
        UserProfileDto userDto=UserProfileDto.builder()
                .name(user.getName())
                .surname(user.getSurname())
                .email(user.getEmail())
                .role(user.getRole().name())
                .company(companyMapper.entityToDto(user.getCompany()))
                .build();
        return userDto;
    }

    @Override
    public UserDto findById(Long id) {
        User user=userRepository.findById(id).orElseThrow(()->new NotFoundException(ErrorCodes.USER_NOT_FOUND.name()));
        UserDto userData=UserDto.builder()
                .name(user.getName())
                .surname(user.getSurname())
                .email(user.getEmail())
                .build();
        return userData;
    }

    @Override
    public UserDto update(Long id, UserDto userDto) {
        User user=userRepository.findById(id).orElseThrow(()->new NotFoundException(ErrorCodes.USER_NOT_FOUND.name()));
        if(userDto.getName()!=null){
            user.setName(userDto.getName());
        }
        if(userDto.getSurname()!=null){
            user.setSurname(userDto.getSurname());
        }
        if(userDto.getEmail()!=null){
            user.setEmail(userDto.getEmail());
        }
        userRepository.save(user);
        UserDto userData=UserDto.builder()
                .name(user.getName())
                .surname(user.getSurname())
                .email(user.getEmail())
                .build();

        return userData;
    }

    @Override
    public void delete(Long id) {
        User user=userRepository.findById(id).orElseThrow(()->new NotFoundException(ErrorCodes.USER_NOT_FOUND.name()));
        userRepository.delete(user);
    }

}

