package az.ingress.springbootsecurity.service.impl;



import az.ingress.springbootsecurity.config.security.JwtService;
import az.ingress.springbootsecurity.dto.CreateTaskDto;
import az.ingress.springbootsecurity.dto.TaskDto;
import az.ingress.springbootsecurity.enums.Role;
import az.ingress.springbootsecurity.enums.Status;
import az.ingress.springbootsecurity.exception.ErrorCodes;
import az.ingress.springbootsecurity.exception.MissingPermission;
import az.ingress.springbootsecurity.exception.NotFoundException;
import az.ingress.springbootsecurity.mapper.TaskMapper;
import az.ingress.springbootsecurity.model.Company;
import az.ingress.springbootsecurity.model.EmailDetails;
import az.ingress.springbootsecurity.model.Task;
import az.ingress.springbootsecurity.model.User;
import az.ingress.springbootsecurity.repository.TaskRepository;
import az.ingress.springbootsecurity.repository.UserRepository;
import az.ingress.springbootsecurity.service.TaskService;
import az.ingress.springbootsecurity.util.EmailSenderService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;



@Service
@RequiredArgsConstructor
public class TaskServiceImpl implements TaskService {
    private final TaskRepository taskRepository;
    private final UserRepository userRepository;
    private final TaskMapper taskMapper;
    private final JwtService jwtService;
    private final EmailSenderService emailSenderService;
    @Override
    public CreateTaskDto createTask(CreateTaskDto createTaskDto) {
        User currentUser=jwtService.getSignedUser();
        List<User> userList=new ArrayList<>();
        for(Long id: createTaskDto.getAssignedUserId()){
            User user=userRepository.findById(id).orElseThrow(() -> new NotFoundException("User not found"));
            userList.add(user);
        }
        Task createdTask=Task.builder()
                .title(createTaskDto.getTitle())
                .description(createTaskDto.getDescription())
                .status(Status.ACTIVATED)
                .createdBy(currentUser)
                .company(currentUser.getCompany())
                .deadline(createTaskDto.getDeadline())
                .userList(userList)
                .build();
         taskRepository.save(createdTask);
         for(User user:userList){
             System.out.println(user.getEmail());
             EmailDetails emailDetails=EmailDetails.builder()
                             .recipient(user.getEmail())
                     .subject("eTaskify")
                     .msgBody("Assigned new task")
             .build();
             emailSenderService.sendMail(emailDetails);
         }
        return null;
    }

    @Override
    public TaskDto findById(Long id) {
        Task task=taskRepository.findById(id).orElseThrow(()->new NotFoundException(ErrorCodes.TASK_NOT_FOUND.name()));
        return taskMapper.entityToDto(task);
    }

    @Override
    public TaskDto update(Long id, TaskDto taskDto) {
        User signedUser=jwtService.getSignedUser();

        Task task=taskRepository.findById(id).orElseThrow(()->new NotFoundException(ErrorCodes.TASK_NOT_FOUND.name()));

        if(signedUser.getId()!=task.getCreatedBy().getId() && !signedUser.getRole().equals(Role.ADMIN.toString())){
            throw  new MissingPermission(ErrorCodes.MISSING_PERMISSION.name());
        }
        if(taskDto.getTitle()!=null){
            task.setTitle(taskDto.getTitle());
        }
        if(taskDto.getDescription()!=null){
            task.setDescription(taskDto.getDescription());
        }
        if(taskDto.getDeadline()!=null){
            task.setDeadline(taskDto.getDeadline());
        }

         taskRepository.save(task);
         return taskMapper.entityToDto(task);
    }

    @Override
    public List<TaskDto> findAll() {
        List<Task> taskList=taskRepository.findAll();
        if(taskList==null){
            throw new NotFoundException(ErrorCodes.NOT_FOUND.name());
        }
        return taskMapper.listEntityToDto(taskList);
    }

    @Override
    public void delete(Long id) {
        User signedUser=jwtService.getSignedUser();
        Task task=taskRepository.findById(id).orElseThrow(()->new NotFoundException(ErrorCodes.TASK_NOT_FOUND.name()));
        if(signedUser.getId()!=task.getCreatedBy().getId() && !signedUser.getRole().equals(Role.ADMIN.toString())){
            throw  new MissingPermission(ErrorCodes.MISSING_PERMISSION.name());
        }
        taskRepository.delete(task);
    }

}

