package az.ingress.springbootsecurity.service;


import az.ingress.springbootsecurity.dto.CreateTaskDto;
import az.ingress.springbootsecurity.dto.TaskDto;

import java.util.List;

public interface TaskService {
    CreateTaskDto createTask(CreateTaskDto createTaskDto);
    TaskDto findById(Long id);
    TaskDto update(Long id,TaskDto taskDto);
    List<TaskDto> findAll();
    void delete(Long id);
   // List<CreateTaskDto> getList();
}
