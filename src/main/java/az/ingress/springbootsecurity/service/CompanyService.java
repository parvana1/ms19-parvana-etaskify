package az.ingress.springbootsecurity.service;

import az.ingress.springbootsecurity.dto.*;

import java.util.List;


public interface CompanyService {
    CompanyDto getCompanyById(Long id);
    //CompanyDto create(CompanyDto companyDto);
    CompanyDto update(Long id,CompanyDto companyDto);
    void delete(Long id);
    ResponseSignUpDto signUp(SignUpDto signUpDto);
    List<CompanyDto> findAll();
     List<TaskDto> getTasks(Long id);
    List<UserDto> getUsers(Long id);
}


