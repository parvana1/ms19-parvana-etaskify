package az.ingress.springbootsecurity.controller;


import az.ingress.springbootsecurity.dto.CompanyDto;
import az.ingress.springbootsecurity.dto.CreateTaskDto;
import az.ingress.springbootsecurity.dto.TaskDto;
import az.ingress.springbootsecurity.dto.UserDto;
import az.ingress.springbootsecurity.service.TaskService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/task")
@RequiredArgsConstructor
public class TaskController {
    private final TaskService taskService;
    @PostMapping("/create")
    public void createTask(@RequestBody CreateTaskDto createTaskDto){
        taskService.createTask(createTaskDto);
    }

    @GetMapping("/{id}")
    public TaskDto getTask(@PathVariable Long id) {
        return  taskService.findById(id);
    }
    @GetMapping("/getList")
    public List<TaskDto> all() {
        return taskService.findAll();
    }
    @PutMapping("/update/{id}")
    public TaskDto update(@PathVariable Long id, @RequestBody TaskDto taskDto){
        return  taskService.update(id,taskDto);
    }

    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable Long id) {
        taskService.delete(id);
    }

}
