package az.ingress.springbootsecurity.controller;


import az.ingress.springbootsecurity.dto.CompanyDto;
import az.ingress.springbootsecurity.dto.TaskDto;
import az.ingress.springbootsecurity.dto.UserDto;
import az.ingress.springbootsecurity.service.CompanyService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/company")
public class CompanyController {
 private final CompanyService companyService;
    public CompanyController(CompanyService companyService) {
        this.companyService = companyService;
    }
    @PutMapping("/update/{id}")
    public CompanyDto updateCompany(@PathVariable Long id,@RequestBody CompanyDto companyDto){
        return  companyService.update(id,companyDto);
    }

    @GetMapping("/getList")
    public List<CompanyDto> all() {
        return companyService.findAll();
    }

    @GetMapping("/{id}")
    public CompanyDto findById(@PathVariable Long id) {
        return companyService.getCompanyById(id);
    }


    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable Long id) {
        companyService.delete(id);
    }

    @GetMapping("/getTasks/{id}")
    public List<TaskDto> findCompanyAllTasks(@PathVariable Long id) {
        return companyService.getTasks(id);
    }

    @GetMapping("/getUsers/{id}")
    public List<UserDto> findCompanyAllUsers(@PathVariable Long id) {
        return companyService.getUsers(id);
    }

}
