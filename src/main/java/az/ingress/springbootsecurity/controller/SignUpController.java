package az.ingress.springbootsecurity.controller;


import az.ingress.springbootsecurity.config.security.JwtService;
import az.ingress.springbootsecurity.dto.*;
import az.ingress.springbootsecurity.service.AuthService;
import az.ingress.springbootsecurity.service.CompanyService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1")
public class SignUpController {
    private final AuthService authService;
    private final JwtService jwtService;
    private final CompanyService companyService;

    public SignUpController(AuthService authService, CompanyService companyService,JwtService jwtService) {
        this.authService = authService;
        this.companyService = companyService;
        this.jwtService=jwtService;
    }

    @PostMapping("/signup")
    public ResponseSignUpDto create(@RequestBody SignUpDto signUpDto){
        return  companyService.signUp(signUpDto);
    }

    @PostMapping("/login")
    public TokenResponseDto login(@RequestBody LoginRequestDto dto){
        return authService.login (dto);
    }
    @PostMapping("/refresh/token")
    public TokenResponseDto refreshToken(@RequestBody RefreshTokenRequestDto token){
        return jwtService.refreshToken(token);
    }

}
