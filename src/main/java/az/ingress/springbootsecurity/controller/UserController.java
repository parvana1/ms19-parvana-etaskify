package az.ingress.springbootsecurity.controller;

import az.ingress.springbootsecurity.dto.UserDto;
import az.ingress.springbootsecurity.dto.UserProfileDto;
import az.ingress.springbootsecurity.model.User;
import az.ingress.springbootsecurity.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;



@RestController
@RequestMapping("/api/v1/user")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;
    @PostMapping("/create")
    public void add(@RequestBody UserDto dto){
        userService.createUser(dto);
    }

    @GetMapping("/profile")
    public UserProfileDto getProfile() {
      return  userService.profile();
    }
    @GetMapping("/{id}")
    public UserDto getUser(@PathVariable Long id) {
        return  userService.findById(id);
    }
    @PutMapping("/update/{id}")
    public UserDto update(@PathVariable Long id, @RequestBody UserDto userDto){
        return  userService.update(id,userDto);
    }
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable Long id) {
        userService.delete(id);
    }
}
