package az.ingress.springbootsecurity.exception;

public class MissingPermission extends RuntimeException{
    public MissingPermission(String message){
        super(message);
    }
}
