package az.ingress.springbootsecurity.exception;


import lombok.extern.slf4j.Slf4j;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import java.time.OffsetDateTime;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(NotFoundException.class)
    public final ResponseEntity<ErrorMessage> handle(NotFoundException ex, WebRequest request){
        log.error("Not found: {}");
        final ErrorMessage response = ErrorMessage
                .builder()
                .message(ex.getMessage())
                .detail(request.getDescription(false))
                .code(ErrorCodes.NOT_FOUND.code)
                .status(HttpStatus.NOT_FOUND.value())
                .path(((ServletWebRequest) request).getRequest().getRequestURI())
                .timestamp(OffsetDateTime.now().toString())
                .build();
        return ResponseEntity.status(404).body(response);

    }

    @ExceptionHandler(AlreadyExistException.class)
    public final ResponseEntity<ErrorMessage> handle(AlreadyExistException ex, WebRequest request){
        log.error("ALREADY EXIST: {}");
        final ErrorMessage response = ErrorMessage
                .builder()
                .message(ex.getMessage())
                .detail(request.getDescription(false))
                .code(ErrorCodes.ALREADY_EXIST.code)
                .path(((ServletWebRequest) request).getRequest().getRequestURI())
                .timestamp(OffsetDateTime.now().toString())
                .build();
        return ResponseEntity.status(404).body(response);

    }
    @ExceptionHandler(BadRequestException.class)
    public final ResponseEntity<ErrorMessage> handle(BadRequestException ex, WebRequest request){
        log.error("Bad Request");
        final ErrorMessage response = ErrorMessage
                .builder()
                .message("Bad Request")
                .detail("Check input")
                .code(ErrorCodes.BAD_REQUEST.code)
                .status(400)
                .path(((ServletWebRequest) request).getRequest().getRequestURI())
                .timestamp(OffsetDateTime.now().toString())
                .build();
        return ResponseEntity.status(400).body(response);

    }

    @ExceptionHandler(PasswordMismatch.class)
    public final ResponseEntity<ErrorMessage> handle(PasswordMismatch ex, WebRequest request){
        final ErrorMessage response = ErrorMessage
                .builder()
                .message("Password mismatch")
                .detail("Check input")
                .code(ErrorCodes.PASSWORD_MISMATCH.code)
                .status(400)
                .path(((ServletWebRequest) request).getRequest().getRequestURI())
                .timestamp(OffsetDateTime.now().toString())
                .build();
        return ResponseEntity.status(400).body(response);

    }

    @ExceptionHandler(MissingPermission.class)
    public final ResponseEntity<ErrorMessage> handle(MissingPermission ex, WebRequest request){
        final ErrorMessage response = ErrorMessage
                .builder()
                .message(ex.getMessage())
                .detail(request.getDescription(false))
                .code(ErrorCodes.MISSING_PERMISSION.code)
                .path(((ServletWebRequest) request).getRequest().getRequestURI())
                .timestamp(OffsetDateTime.now().toString())
                .build();
        return ResponseEntity.status(404).body(response);

    }

}
