package az.ingress.springbootsecurity.exception;

public class AlreadyExistException extends RuntimeException{
    public AlreadyExistException(String message){

        super(message);
    }
}
