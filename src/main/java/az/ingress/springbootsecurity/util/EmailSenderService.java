package az.ingress.springbootsecurity.util;

import az.ingress.springbootsecurity.model.EmailDetails;

public interface EmailSenderService {
    String sendMail(EmailDetails details);
}
