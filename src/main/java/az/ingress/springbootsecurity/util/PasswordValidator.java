package az.ingress.springbootsecurity.util;

import org.springframework.stereotype.Service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
@Service
public class PasswordValidator {

    private static final String PASSWORD_PATTERN ="^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{6,20}$";
    private static final int PASSWORD_LENGTH = 6;
    private static final Pattern pattern = Pattern.compile(PASSWORD_PATTERN);

    public  boolean isValid(String password) {
        boolean b=false;
        if (password.length() < PASSWORD_LENGTH) {
            return b;
        }else{
            Matcher matcher = pattern.matcher(password);
            if(matcher.matches()){
                return true;
            }
            return false;
        }
    }
}