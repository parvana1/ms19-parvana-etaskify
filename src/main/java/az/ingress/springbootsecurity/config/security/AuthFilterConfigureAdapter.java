package az.ingress.springbootsecurity.config.security;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service
public class AuthFilterConfigureAdapter extends SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSecurity> {
 private final AuthFilter authFilter;

    @Override
    public void configure(HttpSecurity http)  {
        log.info("filter AuthFilterConfigureAdapter added");
       http.addFilterBefore(authFilter, UsernamePasswordAuthenticationFilter.class);
    }
}

