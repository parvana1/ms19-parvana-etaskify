package az.ingress.springbootsecurity.config.security;

import az.ingress.springbootsecurity.dto.RefreshTokenRequestDto;
import az.ingress.springbootsecurity.dto.TokenResponseDto;
import az.ingress.springbootsecurity.exception.ErrorCodes;
import az.ingress.springbootsecurity.exception.NotFoundException;
import az.ingress.springbootsecurity.model.User;
import az.ingress.springbootsecurity.repository.UserRepository;
import io.jsonwebtoken.*;
import io.jsonwebtoken.security.Keys;
import jakarta.annotation.PostConstruct;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;


import java.security.Key;
import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class JwtService {
    private static final String BEARER_AUTH_HEADER="Bearer";
    private final UserRepository userRepository;
    private Key key;
    @Value("${security.jwtProperties.secret}")
    private String secretKey;


    @PostConstruct
    public void init(){
        byte[] keyBytes= Base64.getDecoder().decode(secretKey);
        key= Keys.hmacShaKeyFor(keyBytes);
    }

    public TokenResponseDto issueToken(User user){
        TokenResponseDto response =new TokenResponseDto();
        String accessToken  = Jwts.builder()
                .setSubject(user.getName())
                .setIssuedAt(new Date())
                .setExpiration(Date.from(Instant.now().plus(Duration.ofMinutes(20))))
                .signWith(key, SignatureAlgorithm.HS256)
                .claim("role",user.getRole())
                .claim("userId",user.getId())
                .claim("companyId",user.getCompany().getId())
                .compact();
        String refreshToken=Jwts.builder()
                .setSubject(user.getName())
                .setIssuedAt(new Date())
                .setExpiration(Date.from(Instant.now().plus(Duration.ofMinutes(25))))
                .signWith(key, SignatureAlgorithm.HS256)
                .claim("userId",user.getId())
                .compact();
        response.setAccessToken(accessToken);
        response.setRefreshToken(refreshToken);

        return response;
    }

    public Optional<? extends Authentication> getAuthentication(HttpServletRequest request){
        String authorization=request.getHeader("Authorization");
        if(authorization==null){
            return Optional.empty();
        }
        String token =authorization.substring(BEARER_AUTH_HEADER.length()).trim();
        Jws<Claims> claimsJws=Jwts.parser()
                .setSigningKey(key)
                .build()
                .parseSignedClaims(token);
       Claims body= claimsJws.getBody();
        if(body.getExpiration().before(new Date())){
            throw new IllegalArgumentException("token expired");
        }
        return Optional.of(getAuthenticationBearer(body));
    }

    public Authentication getAuthenticationBearer(Claims claims){
        List<String> roles = new ArrayList<>();
        roles.add(claims.get("role",String.class));
        var authorityList = roles
                .stream()
                .map(role -> new SimpleGrantedAuthority(role))
                .collect(Collectors.toList());
        var details = new org.springframework.security.core.userdetails.User(
                claims.getSubject(),
                "",
                authorityList
        );
        return new UsernamePasswordAuthenticationToken(claims.getSubject(), details, authorityList);

    }

    public TokenResponseDto refreshToken(RefreshTokenRequestDto token){
        Claims body= getClaims(token.getToken());
        if(body.getExpiration().before(new Date())){
            throw new IllegalArgumentException("token expired");
        }
        User signedUser=userRepository.findByName(body.getSubject()).orElseThrow(()-> new NotFoundException(ErrorCodes.NOT_FOUND.name()));

        return issueToken(signedUser);
    }

    public Claims  getClaims(String token){
        Jws<Claims> claimsJws=Jwts.parser()
                .setSigningKey(key)
                .build()
                .parseSignedClaims(token);
        return claimsJws.getBody();
    }
    public User getSignedUser(){
          SecurityContext context = SecurityContextHolder.getContext();
          Authentication authentication = context.getAuthentication();
          String currentUserName = authentication.getPrincipal().toString();
          User user=userRepository.findByName(currentUserName).orElseThrow(()-> new NotFoundException(ErrorCodes.NOT_FOUND.name()));
          return user;

    }

}
