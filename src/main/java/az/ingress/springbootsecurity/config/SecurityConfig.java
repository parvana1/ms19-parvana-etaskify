package az.ingress.springbootsecurity.config;

import az.ingress.springbootsecurity.config.security.AuthFilterConfigureAdapter;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;



@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
//@EnableAutoConfiguration(exclude = { WebMvcAutoConfiguration.class })
public class SecurityConfig {
    private final AuthFilterConfigureAdapter authFilterConfigureAdapter;
    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception{
        http.csrf().disable();
        http.cors().disable();
        http.authorizeHttpRequests((auth)->auth.requestMatchers("/api/v1/signup").permitAll());
        http.authorizeHttpRequests((auth)->auth.requestMatchers("/api/v1/login").permitAll());
        http.authorizeHttpRequests((auth)->auth.requestMatchers("/api/v1/refresh/token").permitAll());
        http.authorizeHttpRequests((auth)->auth.requestMatchers("/api/v1/user/delete").hasAuthority("ADMIN"));
        http.authorizeHttpRequests((auth)->auth.requestMatchers("/api/v1/company/{id}").hasAnyAuthority("USER","ADMIN"));
        http.authorizeHttpRequests((auth)->auth.requestMatchers("/api/v1/company/update/{id}").hasAuthority("ADMIN"));
        http.authorizeHttpRequests((auth)->auth.requestMatchers("/api/v1/company/getList").hasAnyAuthority("ADMIN","USER"));
        http.authorizeHttpRequests((auth)->auth.requestMatchers("/api/v1/company/getTasks/{id}").hasAnyAuthority("ADMIN","USER"));
        http.authorizeHttpRequests((auth)->auth.requestMatchers("/api/v1/company/getUsers/{id}").hasAnyAuthority("ADMIN","USER"));
        http.authorizeHttpRequests((auth)->auth.requestMatchers("/api/v1/company/delete/{id}").hasAuthority("ADMIN"));
        http.authorizeHttpRequests((auth)->auth.requestMatchers("/api/v1/user/create").hasAuthority("ADMIN"));
        http.authorizeHttpRequests((auth)->auth.requestMatchers("/api/v1/user/delete/{id}").hasAuthority("ADMIN"));
        http.authorizeHttpRequests((auth)->auth.requestMatchers("/api/v1/user/update/{id}").hasAuthority("ADMIN"));
        http.authorizeHttpRequests((auth)->auth.requestMatchers("/api/v1/user/{id}").hasAnyAuthority("USER","ADMIN"));
        http.authorizeHttpRequests((auth)->auth.requestMatchers("/api/v1/user/profile").hasAnyAuthority("USER","ADMIN"));
        http.authorizeHttpRequests((auth)->auth.requestMatchers("/api/v1/task/delete/{id}").hasAnyAuthority("USER","ADMIN"));
        http.authorizeHttpRequests((auth)->auth.requestMatchers("/api/v1/task/create").hasAuthority("USER"));
        http.authorizeHttpRequests((auth)->auth.requestMatchers("/api/v1/task/getList").hasAnyAuthority("USER","ADMIN"));
        http.authorizeHttpRequests((auth)->auth.requestMatchers("/api/v1/task/{id}").hasAnyAuthority("USER","ADMIN"));
        http.authorizeHttpRequests((auth)->auth.requestMatchers("/api/v1/task/update/{id}").hasAnyAuthority("USER","ADMIN"));;
        http.authorizeHttpRequests((auth)->auth.anyRequest().authenticated());
        http.apply(authFilterConfigureAdapter);
        return http.build();
    }

    @Bean
    public PasswordEncoder encoder(){
        return new BCryptPasswordEncoder();
    }
}
